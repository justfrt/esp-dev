# esp-dev

Docker based toolset for easier dev workflow with the ESP8266 and MicroPython.  
Integrated Scripts to:  
* flash firmware  
* add / remove scripts  
* list scripts on the ESP  
* run specific scritp on the ESP.